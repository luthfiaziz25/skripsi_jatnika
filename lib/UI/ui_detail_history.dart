import 'package:apmakor/UI/ui_menu.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';

class DetailHistory extends StatefulWidget {
  final ph;
  final kekeruhan;
  final tds;
  final waktu;
  DetailHistory({Key key,this.ph,this.kekeruhan,this.tds,this.waktu}) : super(key: key);
  @override
  _DetailHistoryState createState() => _DetailHistoryState();
}

class _DetailHistoryState extends State<DetailHistory> {
  var touchedIndex1;
  var touchedIndex2;
  var touchedIndex3;
  var ph;
  var kekeruhan;
  var tds;
  var waktu;
  double persenph;
  double persentds;
  double persenkekeruhan;

  double sisaph;
  double sisatds;
  double sisakeruh;
  countData(){
    setState(() {
      ph = int.parse('${widget.ph}');
      assert(ph is int);
      kekeruhan = int.parse('${widget.kekeruhan}');
      assert(kekeruhan is int);
      tds = int.parse('${widget.tds}');
      assert(tds is int);
      waktu = widget.waktu;
    });

    persenph = ph / 14 * 100;
    persentds = tds / 5000 * 100;
    persenkekeruhan = kekeruhan / 10 * 100;
    sisaph = 100 - persenph;
    sisatds = 100 - persentds;
    sisakeruh = 100 - persenkekeruhan;
  }
  @override
  void initState() {
    countData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Pigment.fromString('7f7fff'),
      backgroundColor: Pigment.fromString('f7f5fa'),
      appBar : AppBar(
        backgroundColor: Pigment.fromString("#6666ff"),
        title: Text("History"),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))),
        leading: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
          ),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              child: Container(
                alignment: Alignment.center,
                child: Icon(IcoFontIcons.simpleLeft, size: 25,color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              splashColor: Colors.amberAccent,
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                height: MediaQuery.of(context).size.height/7 + 12,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(width: 0.5,color: Colors.black54),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Icon(IcoFontIcons.square,color: Colors.blue[800]),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text("pH : Range 0 - 14",style: TextStyle(color: Colors.black54),),
                      )
                    ]),
                    Row(children: <Widget>[
                      Icon(IcoFontIcons.square,color: Colors.red[800]),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text("Kekeruhan : Range 0 - 10",style: TextStyle(color: Colors.black54)),
                      )
                    ]),
                    Row(children: <Widget>[
                      Icon(IcoFontIcons.square,color: Colors.amber[800]),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text("TDS : Range 0 - 5000",style: TextStyle(color: Colors.black54)),
                      )
                    ]),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: MediaQuery.of(context).size.height/1.2,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height/4,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.centerLeft,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 7,
                            child: PieChart(
                              PieChartData(
                                  pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                                    setState(() {
                                      if (pieTouchResponse.touchInput is FlLongPressEnd ||
                                          pieTouchResponse.touchInput is FlPanEnd) {
                                        touchedIndex1 = -1;
                                      } else {
                                        touchedIndex1 = pieTouchResponse.touchedSectionIndex;
                                      }
                                    });
                                  }),
                                  borderData: FlBorderData(
                                    show: false,
                                  ),
                                  sectionsSpace: 0,
                                  centerSpaceRadius: 15,
                                  sections: chartpH()),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: SizedBox(
                              width: 10,
                            ),
                          ),
                          Expanded(
                            flex: 7,
                            child: Padding(
                              padding: const EdgeInsets.only(top:15,bottom: 15),
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                                height: MediaQuery.of(context).size.height/8,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.blue
                                ),
                                child: Text("OK",style: TextStyle(color: Colors.white),),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height/4,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.centerLeft,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 7,
                            child: PieChart(
                              PieChartData(
                                  pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                                    setState(() {
                                      if (pieTouchResponse.touchInput is FlLongPressEnd ||
                                          pieTouchResponse.touchInput is FlPanEnd) {
                                        touchedIndex3 = -1;
                                      } else {
                                        touchedIndex3 = pieTouchResponse.touchedSectionIndex;
                                      }
                                    });
                                  }),
                                  borderData: FlBorderData(
                                    show: false,
                                  ),
                                  sectionsSpace: 0,
                                  centerSpaceRadius: 15,
                                  sections: chartkekeruhan()),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: SizedBox(
                              width: 10,
                            ),
                          ),
                          Expanded(
                            flex: 7,
                            child: Padding(
                              padding: const EdgeInsets.only(top:15,bottom: 15),
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                                height: MediaQuery.of(context).size.height/8,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.red
                                ),
                                child: Text("OK",style: TextStyle(color: Colors.white)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height/4,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.centerLeft,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 7,
                            child: PieChart(
                              PieChartData(
                                  pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                                    setState(() {
                                      if (pieTouchResponse.touchInput is FlLongPressEnd ||
                                          pieTouchResponse.touchInput is FlPanEnd) {
                                        touchedIndex2 = -1;
                                      } else {
                                        touchedIndex2 = pieTouchResponse.touchedSectionIndex;
                                      }
                                    });
                                  }),
                                  borderData: FlBorderData(
                                    show: false,
                                  ),
                                  sectionsSpace: 0,
                                  centerSpaceRadius: 15,
                                  sections: charttds()),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: SizedBox(
                              width: 10,
                            ),
                          ),
                          Expanded(
                            flex: 7,
                            child: Padding(
                              padding: const EdgeInsets.only(top:15,bottom: 15),
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                                height: MediaQuery.of(context).size.height/8,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.amber
                                ),
                                child: Text("OK",style: TextStyle(color: Colors.white)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  List<PieChartSectionData> chartpH() {
    return List.generate(2, (i) {
      final isTouched = i == touchedIndex1;
      final double fontSize = isTouched ? 20 : 16;
      final double radius = isTouched ? 55 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.blue[800],
            value: persenph,
            title: '${persenph.roundToDouble()} %',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: Colors.blue[300],
            value: sisaph,
            title: '${sisaph.roundToDouble()} %',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
  List<PieChartSectionData> charttds() {
    return List.generate(2, (i) {
      final isTouched = i == touchedIndex2;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.amber[800],
            value: persentds,
            title: '${persentds.roundToDouble()} %',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: Colors.amber[300],
            value: sisatds,
            title: '${sisatds.roundToDouble()} %',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
  List<PieChartSectionData> chartkekeruhan() {
    return List.generate(2, (i) {
      final isTouched = i == touchedIndex3;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.red[800],
            value: persenkekeruhan,
            title: '${persenkekeruhan.roundToDouble()} %',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: Colors.red[300],
            value: sisakeruh,
            title: '${sisakeruh.roundToDouble()} %',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
}

class CustomClipPath extends CustomClipper<Path> {
  var radius=10.0;
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width,0);
    path.lineTo(0,size.height);
    path.quadraticBezierTo(size.width - 10, size.height, size.width, size.height);

    return path;
  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
