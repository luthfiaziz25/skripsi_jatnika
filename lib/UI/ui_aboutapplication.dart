import 'dart:convert';
import 'dart:io';

import 'package:apmakor/Database/DatabaseLocal.dart';
import 'package:apmakor/UI/ui_image_preview.dart';
import 'package:apmakor/UI/ui_menu.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';

class AboutAplication extends StatefulWidget {
  @override
  _AboutAplicationState createState() => _AboutAplicationState();
}

class _AboutAplicationState extends State<AboutAplication> {
  String base64Image;
  String baseFromLocal;
  var FileUpload;
  void readImage(){
    SaveLocalStorage().readImage().then((value) async {
      print(value);
      baseFromLocal = value;
      setState(() {
        final FileFromStorage = base64Decode(baseFromLocal);
        FileUpload = FileFromStorage;
      });
      return await FileUpload;
    });
  }
  Future<bool> _onWillPop() async {
    return  showDialog(context: context,builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Pigment.fromString('#e9dcfc'),
        title: Text("Anda Ingin Keluar ?",style: TextStyle(color: Colors.black54),),
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        content: Container(
          height: 35,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.black, width: 0.1),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white,fontSize: 12),
                      ),
                    )),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.2),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        exit(0);
//                         _showAlertSayGoodBye(context);
                      },
                      child: Text("Lanjutkan",style: TextStyle(fontSize: 12,color: Colors.black54)),
                    )),
              )
            ],
          ),
        ),
      );
    }) ?? false;
  }
  @override
  void initState() {
    // TODO: implement initState
    readImage();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
//      backgroundColor: Pigment.fromString('7f7fff'),
        backgroundColor: Pigment.fromString('f7f5fa'),
        appBar: AppBar(
          backgroundColor: Pigment.fromString("#6666ff"),
          title: Text("About Application"),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))),
          leading:  Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
            ),
            child: FlatButton(
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  child: Icon(IcoFontIcons.navigationMenu, size: 25,color: Colors.white,),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                splashColor: Colors.amberAccent,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> OptionMenu()));
                }),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(20),
          child: Container(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child : CircleAvatar(
                    radius: 70,
                    backgroundImage: AssetImage("lib/assets/apmakor_icon.png"),
                  )
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  flex: 1,
                  child: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",style: TextStyle(

                  ),textAlign: TextAlign.justify),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
