import 'dart:convert';
import 'dart:io';

import 'package:apmakor/Component/firebase_massaging.dart';
import 'package:apmakor/Component/linechart.dart';
import 'package:apmakor/Component/linechartkeruh.dart';
import 'package:apmakor/Component/linecharttds.dart';
import 'package:apmakor/Database/DatabaseLocal.dart';
import 'package:apmakor/Service/push_notification.dart';
import 'package:apmakor/UI/ui_image_preview.dart';
import 'package:apmakor/UI/ui_menu.dart';
import 'package:background_fetch/background_fetch.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';
import 'package:fl_chart/fl_chart.dart';


class Beranda extends StatefulWidget {
  @override
  _BerandaState createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  List<Color> gradientColorsph = [
    Colors.blue[900],
    Colors.lightBlueAccent,
  ];
  List<Color> gradientColorskekeruhan = [
    Colors.red,
    Colors.orange,
  ];
  List<Color> gradientColorstds = [
    Colors.green,
    Colors.yellow,
  ];
  List<FlSpot>valuesph = [];
  List<FlSpot>valueskeruh = [];
  List<FlSpot>valuestds = [];


  final dbRef = FirebaseDatabase.instance.reference();
  List dataFirebase = [];
  List dataTMP = [];
  List ph = [];
  List kekeruhan = [];
  List tds = [];
  List time = [];
  List finalTMP = [];
  var vph;
  var vkekeruhan;
  var vtds;

  var namakolam = "Nama Kolam Tidak Tersedia";
  var jeniskolam = "Indoor / Outdoor";
  double panjang = 0.0;
  double lebar = 0.0;
  double dalam = 0.0;

  String base64Image;
  String baseFromLocal;
  var FileUpload;
  var _token;

  List phGrafik = [];


  final firebaseMessaging = FirebaseMessaging();
  Widget viewBeranda(ph,kekeruhan,tds,time){
   /* tmptime.clear();
    valuesph.clear();
    valueskeruh.clear();
    valuestds.clear();*/
    if(dataTMP.isEmpty){
      vph = 0;
      vkekeruhan = 0;
      vtds = 0;
    }else{
      for(int i=0;i<ph.length;i++){
        vph = ph[i];
      }
      for(int i=0;i<kekeruhan.length;i++){
        vkekeruhan = kekeruhan[i];
      }
      for(int i=0;i<tds.length;i++){
        vtds = tds[i];
      }
    }
    /*if(time.isNotEmpty){
      tmptime = time.toSet().toList();
      valuesph.add(FlSpot(0.0,0.0));
      valueskeruh.add(FlSpot(0.0,0.0));
      valuestds.add(FlSpot(0.0,0.0));
      for(int i=0;i<tmptime.length;i++){
        if(i > 9 && (i+1) > 10){
//          valuesph.add(FlSpot(double.parse((i-1).toString()),double.parse(ph[i-1])));
        }else{
          valuesph.add(FlSpot(double.parse((i+1).toString()),double.parse(ph[i])));
          valueskeruh.add(FlSpot(double.parse((i+1).toString()),double.parse(kekeruhan[i])));
          valuestds.add(FlSpot(double.parse((i+1).toString()),double.parse(tds[i])/10));
        }
      }
    }else{
      valuesph.clear();
      valuesph.add(FlSpot(0.0,0.0));

      valueskeruh.clear();
      valueskeruh.add(FlSpot(0.0,0.0));

      valuestds.clear();
      valuestds.add(FlSpot(0.0,0.0));
    }*/
  /*  if(double.parse(vph.toString()) < 4){
      sendnotif().dangernotif();
    }*/
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 4, // has the effect of softening the shadow
                    spreadRadius: 0.03, // has the effect of extending the shadow
                    offset: Offset(
                      2.0, // horizontal, move right 10
                      2.0, // vertical, move down 10
                    ),
                  )
                ],
//                  color: Pigment.fromString("#6666ff")
              ),
              child: Column(
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            FileUpload != null ? Navigator.push(context, MaterialPageRoute(builder: (context)=>ImageShow(FileUpload))) : Container();
                          },
                          child: CircleAvatar(
                            radius: 35,
                            backgroundImage: FileUpload != null ? MemoryImage(
                              FileUpload,
                            ) : AssetImage('lib/assets/emptydata.png'),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width/2,
                                child: Text("$namakolam",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.black54),overflow: TextOverflow.ellipsis,maxLines: 2,)),
                            Container(
                                width: MediaQuery.of(context).size.width/2,
                                child: Text("$jeniskolam",style: TextStyle(fontSize: 11,fontStyle: FontStyle.italic,color: Colors.black54),overflow: TextOverflow.ellipsis,maxLines: 2,))
                          ],
                        )
                      ]),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 65,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 2.5, // has the effect of softening the shadow
                                    spreadRadius: 0.03, // has the effect of extending the shadow
                                    offset: Offset(
                                      2.0, // horizontal, move right 10
                                      2.0, // vertical, move down 10
                                    ),
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("pH",style: TextStyle(color: Colors.black54,fontSize: 15,fontWeight: FontWeight.bold)),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text("${vph}",style: TextStyle(color: Colors.black54,fontSize: 18,fontWeight: FontWeight.bold),overflow: TextOverflow.ellipsis,),
                                        Text("PPM",style: TextStyle(color: Colors.black54,fontSize: 10,fontWeight: FontWeight.bold))
                                      ])
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 2.5, // has the effect of softening the shadow
                                    spreadRadius: 0.03, // has the effect of extending the shadow
                                    offset: Offset(
                                      2.0, // horizontal, move right 10
                                      2.0, // vertical, move down 10
                                    ),
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Turbidity",style: TextStyle(color: Colors.black54,fontSize: 15,fontWeight: FontWeight.bold)),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text("${vkekeruhan}",style: TextStyle(color: Colors.black54,fontSize: 18,fontWeight: FontWeight.bold),overflow: TextOverflow.ellipsis,),
                                        Text("NTU",style: TextStyle(color: Colors.black54,fontSize: 10,fontWeight: FontWeight.bold))
                                      ])
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 2.5, // has the effect of softening the shadow
                                    spreadRadius: 0.03, // has the effect of extending the shadow
                                    offset: Offset(
                                      2.0, // horizontal, move right 10
                                      2.0, // vertical, move down 10
                                    ),
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("TDS",style: TextStyle(color: Colors.black54,fontSize: 15,fontWeight: FontWeight.bold)),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                    Text("${vtds}",style: TextStyle(color: Colors.black54,fontSize: 18,fontWeight: FontWeight.bold),overflow: TextOverflow.ellipsis,),
                                    Text("PPM",style: TextStyle(color: Colors.black54,fontSize: 10,fontWeight: FontWeight.bold))
                                  ])
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 20,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
//                          color: Colors.green[100],
//                          color: Colors.orange[100],
                          color: Colors.red[100],

                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 2.5, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: 20,
//                        width:MediaQuery.of(context).size.width/3.6,
//                        width:MediaQuery.of(context).size.width/1.8,
                        width:MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
//                          color: Colors.green,
//                          color: Colors.orange,
                          color: Colors.red,
                        ),
                        child: Text("TIDAK LAYAK",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
                      )
                    ],
                  )
                ],
              ),
            ),

            SizedBox(
              height: 10,
            ),
            Container(
              child: LineChartPh(),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: LineChartKeruh(),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: LineChartTds(),
            ),
            /*Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 2.0,
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                          color: Colors.white),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 18.0, left: 12.0, top: 24, bottom: 12),
                        child: LineChart(
                          LineChartData(
                            gridData: FlGridData(
                              show: true,
                              drawVerticalLine: true,
                              getDrawingHorizontalLine: (value) {
                                return FlLine(
                                  color: Colors.grey,
                                  strokeWidth: 1,
                                );
                              },
                              getDrawingVerticalLine: (value) {
                                return FlLine(
                                  color: Colors.grey,
                                  strokeWidth: 1,
                                );
                              },
                            ),
                            titlesData: FlTitlesData(
                              show: true,
                              bottomTitles: SideTitles(
                                showTitles: true,
                                reservedSize: 22,
                                textStyle:
                                const TextStyle(color: Color(0xff68737d), fontWeight: FontWeight.bold, fontSize: 16),
                                getTitles: (value) {
                                  switch (value.toInt()) {
                                    case 0:
                                      return '0';
                                    case 10:
                                      return '10';
                                  }
                                  return '';
                                },
                                margin: 5,
                              ),
                              leftTitles: SideTitles(
                                showTitles: true,
                                textStyle: const TextStyle(
                                  color: Color(0xff67727d),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                                getTitles: (value) {
                                  switch (value.toInt()) {
                                    case 1:
                                      return '1';
                                    case 7:
                                      return '7';
                                    case 14:
                                      return '14';
                                  }
                                  return '';
                                },
                                reservedSize: 28,
                                margin: 5,
                              ),
                            ),
                            borderData:
                            FlBorderData(show: true, border: Border.all(color: const Color(0xff37434d), width: 1)),
                            minX: 0,
                            maxX: 10,
                            minY: 0,
                            maxY: 14,
                            lineBarsData: [
                              LineChartBarData(
                                spots: valuesph,
                                isCurved: true,
                                colors: gradientColorsph,
                                barWidth: 3,
                                isStrokeCapRound: true,
                                dotData: FlDotData(
                                  show: false,
                                ),
                                belowBarData: BarAreaData(
                                  show: true,
                                  colors: gradientColorsph.map((color) => color.withOpacity(0.3)).toList(),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Text("Grafik pH 10 Data Terbaru",style:TextStyle(color: Colors.grey[500],fontSize: 10)),
                    ),
                  ),
                ],
              ),
            ),*/
            /*SizedBox(
              height: 10,
            ),
            Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 2.0,
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                          color: Colors.white),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 18.0, left: 12.0, top: 24, bottom: 12),
                        child: LineChart(
                          LineChartData(
                            gridData: FlGridData(
                              show: true,
                              drawVerticalLine: true,
                              getDrawingHorizontalLine: (value) {
                                return FlLine(
                                  color: Colors.grey[400],
                                  strokeWidth: 1,
                                );
                              },
                              getDrawingVerticalLine: (value) {
                                return FlLine(
                                  color: Colors.grey[400],
                                  strokeWidth: 1,
                                );
                              },
                            ),
                            titlesData: FlTitlesData(
                              show: true,
                              bottomTitles: SideTitles(
                                showTitles: true,
                                reservedSize: 22,
                                textStyle:
                                const TextStyle(color: Color(0xff68737d), fontWeight: FontWeight.bold, fontSize: 16),
                                getTitles: (value) {
                                  switch (value.toInt()) {
                                    case 0:
                                      return '0';
                                    case 10:
                                      return '10';
                                  }
                                  return '';
                                },
                                margin: 5,
                              ),
                              leftTitles: SideTitles(
                                showTitles: true,
                                textStyle: const TextStyle(
                                  color: Color(0xff67727d),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                                getTitles: (value) {
                                  switch (value.toInt()) {
                                    case 1:
                                      return '1';
                                    case 13:
                                      return '13';
                                    case 25:
                                      return '25';
                                  }
                                  return '';
                                },
                                reservedSize: 28,
                                margin: 5,
                              ),
                            ),
                            borderData:
                            FlBorderData(show: true, border: Border.all(color: const Color(0xff37434d), width: 1)),
                            minX: 0,
                            maxX: 10,
                            minY: 0,
                            maxY: 25,
                            lineBarsData: [
                              LineChartBarData(
                                spots: valueskeruh,
                                isCurved: true,
                                colors: gradientColorskekeruhan,
                                barWidth: 3,
                                isStrokeCapRound: true,
                                dotData: FlDotData(
                                  show: false,
                                ),
                                belowBarData: BarAreaData(
                                  show: true,
                                  colors: gradientColorskekeruhan.map((color) => color.withOpacity(0.3)).toList(),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Text("Grafik Kekeruhan 10 Data Terbaru",style:TextStyle(color: Colors.grey[500],fontSize: 10)),
                    ),
                  ),
                ],
              ),
            ),*/
            /*SizedBox(
              height: 10,
            ),
            Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 2.0,
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                          color: Colors.white),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 18.0, left: 12.0, top: 24, bottom: 12),
                        child: LineChart(
                          LineChartData(
                            gridData: FlGridData(
                              show: true,
                              drawVerticalLine: true,
                              getDrawingHorizontalLine: (value) {
                                return FlLine(
                                  color: Colors.grey[100],
                                  strokeWidth: 1,
                                );
                              },
                              getDrawingVerticalLine: (value) {
                                return FlLine(
                                  color: Colors.grey[100],
                                  strokeWidth: 1,
                                );
                              },
                            ),
                            titlesData: FlTitlesData(
                              show: true,
                              bottomTitles: SideTitles(
                                showTitles: true,
                                reservedSize: 22,
                                textStyle:
                                const TextStyle(color: Color(0xff68737d), fontWeight: FontWeight.bold, fontSize: 16),
                                getTitles: (value) {
                                  switch (value.toInt()) {
                                    case 0:
                                      return '0';
                                    case 10:
                                      return '10';
                                  }
                                  return '';
                                },
                                margin: 5,
                              ),
                              leftTitles: SideTitles(
                                showTitles: true,
                                textStyle: const TextStyle(
                                  color: Color(0xff67727d),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10,
                                ),
                                getTitles: (value) {
                                  switch (value.toInt()) {
                                    case 1:
                                      return '100';
                                    case 100:
                                      return '1000';
                                    case 200:
                                      return '2000';
                                    case 300:
                                      return '3000';
                                    case 400:
                                      return '4000';
                                    case 500:
                                      return '5000';
                                  }
                                  return '';
                                },
                                reservedSize: 28,
                                margin: 5,
                              ),
                            ),
                            borderData:
                            FlBorderData(show: true, border: Border.all(color: const Color(0xff37434d), width: 1)),
                            minX: 0,
                            maxX: 10,
                            minY: 0,
                            maxY: 500,
                            lineBarsData: [
                              LineChartBarData(
                                spots: valuestds,
                                isCurved: true,
                                colors: gradientColorstds,
                                barWidth: 3,
                                isStrokeCapRound: true,
                                dotData: FlDotData(
                                  show: false,
                                ),
                                belowBarData: BarAreaData(
                                  show: true,
                                  colors: gradientColorstds.map((color) => color.withOpacity(0.3)).toList(),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Text("Grafik TDS 10 Data Terbaru",style:TextStyle(color: Colors.grey[500],fontSize: 10)),
                    ),
                  ),
                ],
              ),
            ),*/
          ],
        ),
      ),
    );
  }
  List<DateTime> _events = [];
  int _status = 0;
  Future<void> initPlatformState() async {
    // Configure BackgroundFetch.
    BackgroundFetch.configure(BackgroundFetchConfig(
        minimumFetchInterval: 10,
        stopOnTerminate: false,
        enableHeadless: false,
        requiresBatteryNotLow: false,
        requiresCharging: false,
        requiresStorageNotLow: false,
        requiresDeviceIdle: false,
        requiredNetworkType: NetworkType.ANY
    ), (String taskId) async {
      // This is the fetch-event callback.
      print("[BackgroundFetch] Event received $taskId");
      setState(() {
        _events.insert(0, new DateTime.now());
      });
      // IMPORTANT:  You must signal completion of your task or the OS can punish your app
      // for taking too long in the background.
      BackgroundFetch.finish(taskId);
    }).then((int status) {
      print('[BackgroundFetch] configure success: $status');
      setState(() {
        _status = status;
      });
    }).catchError((e) {
      print('[BackgroundFetch] configure ERROR: $e');
      setState(() {
        _status = e;
      });
    });

    // Optionally query the current BackgroundFetch status.
    int status = await BackgroundFetch.status;
    setState(() {
      _status = status;
    });
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }
  @override
  void initState() {
    initPlatformState();
    BackgroundFetch.start().then((int status) {
      print('[BackgroundFetch] start success: $status');
    }).catchError((e) {
      print('[BackgroundFetch] start FAILURE: $e');
    });
    firebaseMessaging.subscribeToTopic("apmakor");
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        debugPrint('onMessage: $message');
//        getDataFcm(message);
      },
//      onBackgroundMessage: onBackgroundMessage,
      onResume: (Map<String, dynamic> message) async {
        debugPrint('onResume: $message');
//        getDataFcm(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        debugPrint('onLaunch: $message');
//        getDataFcm(message);
      },
    );
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true, provisional: true),
    );
    firebaseMessaging.onIosSettingsRegistered.listen((settings) {
      debugPrint('Settings registered: $settings');
    });
    notification();
    readData();
    readImage();
    super.initState();
  }
  Future<bool> _onWillPop() async {
    return  showDialog(context: context,builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Pigment.fromString('#e9dcfc'),
        title: Text("Anda Ingin Keluar ?",style: TextStyle(color: Colors.black54),),
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        content: Container(
          height: 35,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.black, width: 0.1),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white,fontSize: 12),
                      ),
                    )),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.2),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        exit(0);
//                         _showAlertSayGoodBye(context);
                      },
                      child: Text("Lanjutkan",style: TextStyle(fontSize: 12,color: Colors.black54)),
                    )),
              )
            ],
          ),
        ),
      );
    }) ?? false;
  }
  void readData(){
    SaveLocalStorage().readData().then((value){
      var body = jsonDecode(value);
      setState(() {
        namakolam = body['namakolam'];
        panjang = double.parse(body['panjang']);
        lebar = double.parse(body['lebar']);
        dalam = double.parse(body['kedalaman']);
        jeniskolam = body['jenis'];
      });
      return value;
    });
  }
  void readImage(){
    SaveLocalStorage().readImage().then((value) async {
      baseFromLocal = value;
      setState(() {
        final FileFromStorage = base64Decode(baseFromLocal);
        FileUpload = FileFromStorage;
      });
      return await FileUpload;
    });
  }
  void notification() {
    PushNotificationsManager().init().then((token) async {
      print("ini token " + token.toString());
      setState(() {
        _token = token;
      });
      return await _token;
    });
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
//        backgroundColor: Pigment.fromString('e9defc'),
        backgroundColor: Pigment.fromString('f7f5fa'),
//        backgroundColor: Pigment.fromString('7f7fff'),
        appBar: AppBar(
          backgroundColor: Pigment.fromString("#6666ff"),
          title: Text("Home"),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))),
            leading:  Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
  //                        color: Colors.amber,
              ),
              child: FlatButton(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    alignment: Alignment.center,
                    child: Icon(IcoFontIcons.navigationMenu, size: 25,color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  splashColor: Colors.amberAccent,
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> OptionMenu()));
                  }),
            ),
        ),
        body: StreamBuilder(
          stream: dbRef.limitToLast(10).onValue,
          builder: (context, AsyncSnapshot<Event> snapshot){
            if (snapshot.hasData) {
              dataTMP.clear();
              DataSnapshot dataValues = snapshot.data.snapshot;
              if(dataValues.value != null){
                dataFirebase.clear();
                Map<dynamic, dynamic> values = dataValues.value;
                values.forEach((key, values) {
                  dataFirebase.add(values);
                });
                for(int i = 0;i<dataFirebase.length;i++){
                  dataTMP.add(dataFirebase[i].toString().split("/"));
                }
                dataTMP.sort((a, b) {
                  return a[3].toLowerCase().compareTo(b[3].toLowerCase());
                });
                ph.clear();
                kekeruhan.clear();
                tds.clear();
                finalTMP = dataTMP.toSet().toList();
                for(int j=0;j<finalTMP.length;j++){
                  ph.add(finalTMP[j][0]);
                  kekeruhan.add(finalTMP[j][1]);
                  tds.add(finalTMP[j][2]);
                  time.add(finalTMP[j][3]);
                }
              }else{
                ph.clear();
                kekeruhan.clear();
                tds.clear();
                dataTMP.clear();
                time.clear();
              }
            }
            return viewBeranda(ph,kekeruhan,tds,time);
          },
        )
      ),
    );
  }
}

