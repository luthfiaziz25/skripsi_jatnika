import 'dart:io';

import 'package:apmakor/UI/ui_aboutapplication.dart';
import 'package:apmakor/UI/ui_beranda.dart';
import 'package:apmakor/UI/ui_data_kolam_renang.dart';
import 'package:apmakor/UI/ui_listhistory.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';

class OptionMenu extends StatefulWidget {
  @override
  _OptionMenuState createState() => _OptionMenuState();
}

class _OptionMenuState extends State<OptionMenu> {
  Future<bool> _onWillPop() async {
    return  showDialog(context: context,builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Pigment.fromString('#e9dcfc'),
        title: Text("Anda Ingin Keluar ?",style: TextStyle(color: Colors.black54),),
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        content: Container(
          height: 35,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.black, width: 0.1),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white,fontSize: 12),
                      ),
                    )),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.1),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        exit(0);
//                         _showAlertSayGoodBye(context);
                      },
                      child: Text("Lanjutkan",style: TextStyle(fontSize: 12,color: Colors.black54)),
                    )),
              )
            ],
          ),
        ),
      );
    }) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
//          backgroundColor: Pigment.fromString('7f7fff'),
          backgroundColor: Pigment.fromString('f7f5fa'),
          appBar: AppBar(
            backgroundColor: Pigment.fromString("#6666ff"),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))
            ),
            title: Text("Menu"),
            leading: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
              ),
              child: FlatButton(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    alignment: Alignment.center,
                    child: Icon(IcoFontIcons.simpleLeft, size: 25,color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  splashColor: Colors.amberAccent,
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: (){
                      Navigator.pop(context);
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Beranda()));
                    },
//                    color: Pigment.fromString('#ccccff'),
                    color: Pigment.fromString('6666ff'),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    splashColor: Colors.lightBlueAccent,
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(width: 0.5,color: Pigment.fromString('#929494')),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
//                        color: Pigment.fromString('#929494'),
                        ),
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height/7 + 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height/10,
                                child: Icon(IcoFontIcons.home,size: 70,color: Colors.white)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                height: MediaQuery.of(context).size.height/25,
                                child: Text("Home",style: TextStyle(fontSize: 20,color: Colors.white),))
                          ],
                        )
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: (){
                      Navigator.pop(context);
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>DataKolam()));
                    },
//                    color: Pigment.fromString('#ccccff'),
                    color: Pigment.fromString('6666ff'),

                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    splashColor: Colors.lightBlueAccent,
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(width: 0.5,color: Pigment.fromString('#929494')),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
//                        color: Pigment.fromString('#929494'),

                        ),
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height/7 + 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height/10,
                                child: Icon(IcoFontIcons.database,size: 70,color: Colors.white)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                height: MediaQuery.of(context).size.height/25,
                                child: Text("Data Kolam Renang",style: TextStyle(fontSize: 20,color: Colors.white),))
                          ],
                        )
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: (){
                      Navigator.pop(context);
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>ListHistory()));
                    },
//                    color: Pigment.fromString('#ccccff'),
                    color: Pigment.fromString('6666ff'),

                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    splashColor: Colors.lightBlueAccent,
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(width: 0.5,color: Pigment.fromString('#929494')),
//                        color: Pigment.fromString('#929494'),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height/7 + 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height/10,
                                child: Icon(IcoFontIcons.history,size: 70,color: Colors.white)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                height: MediaQuery.of(context).size.height/25,
                                child: Text("History",style: TextStyle(fontSize: 20,color: Colors.white),))
                          ],
                        )
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: (){
                      Navigator.pop(context);
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AboutAplication()));
                    },
//                    color: Colors.red,
                    color: Pigment.fromString('6666ff'),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    splashColor: Colors.lightBlueAccent,
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(width: 0.5,color: Pigment.fromString('#929494')),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height/7 + 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height/10,
                                child: Icon(IcoFontIcons.userAlt5,size: 70,color: Colors.white)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                height: MediaQuery.of(context).size.height/25,
                                child: Text("About Aplication",style: TextStyle(fontSize: 20,color: Colors.white),))
                          ],
                        )
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: (){
                      _onWillPop();
                    },
                    color: Colors.deepOrangeAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    splashColor: Colors.lightBlueAccent,
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(width: 0.5,color: Pigment.fromString('#929494')),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 4, // has the effect of softening the shadow
                              spreadRadius: 0.03, // has the effect of extending the shadow
                              offset: Offset(
                                2.0, // horizontal, move right 10
                                2.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height/7 + 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height/10,
                                child: Icon(IcoFontIcons.logout,size: 70,color: Colors.white)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                height: MediaQuery.of(context).size.height/25,
                                child: Text("Close",style: TextStyle(fontSize: 20,color: Colors.white),))
                          ],
                        )
                    ),
                  ),
                ),
                SizedBox(height: 10,)
              ],
            ),
          )
      ),
    );
  }
}
