import 'dart:convert';
import 'dart:io';

import 'package:apmakor/Component/export_pdf.dart';
import 'package:apmakor/Database/DatabaseLocal.dart';
import 'package:apmakor/UI/ui_detail_history.dart';
import 'package:apmakor/UI/ui_menu.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';
import 'package:http/http.dart' as http;

class ListHistory extends StatefulWidget {
  @override
  _ListHistoryState createState() => _ListHistoryState();
}

class _ListHistoryState extends State<ListHistory> {
  List<DropdownMenuItem<String>> _dropDownBulan;
  List<DropdownMenuItem<String>> _dropDownTahun;
  TextEditingController bulanContol = new TextEditingController();
  TextEditingController tahunContol = new TextEditingController();
  var valuebulan;
  var valuetahun;
  bool droptahun = false;
  List _bulan = ['Pilih Bulan','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
  List _tahun = ['Pilih Tahun','2019','2020'];
  void changeBulan(String selected) {
    if (selected != null || selected != '') {
      setState(() {
        Navigator.of(context).pop();
        show();
        valuebulan = selected;
        droptahun = true;

      });
    }
  }
  void changeTahun(String selected) {
    if (selected != null || selected != '') {
      setState(() {
        Navigator.of(context).pop();
        show();
        valuetahun = selected;
      });
    }
  }
  List<DropdownMenuItem<String>> buildAndGetDropDownMenuBulan() {
    List<DropdownMenuItem<String>> items = new List();
    for (String data in _bulan) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(value: data, child: new Text(data)));
    }
    return items;
  }
  List<DropdownMenuItem<String>> buildAndGetDropDownMenuTahun() {
    List<DropdownMenuItem<String>> items = new List();
    for (String data in _tahun) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(value: data, child: new Text(data)));
    }
    return items;
  }
  show() {
    _dropDownBulan = buildAndGetDropDownMenuBulan();
    _dropDownTahun = buildAndGetDropDownMenuTahun();
    valuebulan = _dropDownBulan[0].value;
    valuetahun = _dropDownTahun[0].value;
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          backgroundColor: Pigment.fromString("#e9dcfc"),
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(20)),
          title: new Text("Filter"),
          content: Container(
            height: MediaQuery.of(context).size.height/8,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Container(
                    width: MediaQuery.of(context).size.longestSide,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: bulanContol.text,
                        items: _dropDownBulan,
                        onChanged:(select){
                            if (select.toString() != null || select.toString() != '') {
                              setState(() {
                                Navigator.of(context).pop();
                                show();
                                valuebulan = select.toString();
                                bulanContol.text = select.toString();
                                droptahun = true;
                              });
                            }
                        },
                        hint: Text("Bulan"),
                      ),
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(color: Colors.black38),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    padding: const EdgeInsets.only(left: 10),
                  ),
                ),
                Expanded(flex: 1,
                child: Container(),
                ),
                Expanded(
                  flex: 6,
                  child: Container(
                    width: MediaQuery.of(context).size.longestSide,
                    child: GestureDetector(
                      onTap: (){
                        droptahun == false ?
                        Fluttertoast.showToast(msg: "Pilih Bulan Terlebih Dahulu") : Container();
                      },
                      child: IgnorePointer(
                        ignoring: droptahun? false : true,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: tahunContol.text,
                            items: _dropDownTahun,
                            onChanged:(select){
                              if (select.toString() != null || select.toString() != '') {
                                setState(() {
                                  Navigator.of(context).pop();
                                  show();
                                  tahunContol.text = select.toString();
                                  valuetahun = select.toString();
                                });
                              }
                            },
                            hint: Text("Tahun"),
                          ),
                        ),
                      ),
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(color: Colors.black38),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    padding: const EdgeInsets.only(left: 10),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Submit"),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  filter = "${valuebulan} ${valuetahun}";
                });
              },
            ),
          ],
        );
      }
    );
  }
  Brightness brigdark = Brightness.dark;
  Brightness briglight = Brightness.light;
  List datahasil;
  String filter = "";
  //Part FetchData
  List ph = [];
  List kekeruhan = [];
  List tds = [];
  List date = [];
  List alldata = [];
  List temp = [];
  int lengthdata;
  Map data;

  var nilaipH;
  var nilaitds;
  var nilaikekeruhan;
  var datetime;
  var namakolam = "Nama Kolam Tidak Tersedia";

  final databaseReference = FirebaseDatabase.instance.reference();
  getData() async {
    await databaseReference.once().then((DataSnapshot snapshot) {
      data = snapshot.value;
        for(var list in data.keys){
          alldata.add(data[list]);
        }
        for(int i=0;i<alldata.length;i++) {
          temp.add(alldata[i].toString().split("/"));
        }
        for(int j = 0;j<temp.length;j++){
          setState(() {
            ph.add(temp[j][0]);
            tds.add(temp[j][1]);
            kekeruhan.add(temp[j][2]);
            date.add(temp[j][3]);
          });
        }
        for(int j=0; j< temp.length;j++){
          setState(() {
            nilaipH = temp[j][0];
            nilaitds = temp[j][1];
            nilaikekeruhan = temp[j][2];
            datetime = temp[j][3];
          });
        }
        setState(() {
          lengthdata = temp.length;
        });
     return temp;
    });
  }
  Future<bool> _onWillPop() async {
    return  showDialog(context: context,builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Pigment.fromString('#e9dcfc'),
        title: Text("Anda Ingin Keluar ?",style: TextStyle(color: Colors.black54),),
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        content: Container(
          height: 35,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.black, width: 0.1),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white,fontSize: 12),
                      ),
                    )),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.2),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        exit(0);
//                         _showAlertSayGoodBye(context);
                      },
                      child: Text("Lanjutkan",style: TextStyle(fontSize: 12,color: Colors.black54)),
                    )),
              )
            ],
          ),
        ),
      );
    }) ?? false;
  }
  void readData(){
    SaveLocalStorage().readData().then((value){
      var body = jsonDecode(value);
      setState(() {
        namakolam = body['namakolam'];
//        panjang = body['panjang'];
//        lebar = body['lebar'];
//        dalam = body['kedalaman'];
//        jeniskolam = body['jenis'];
      });
      return value;
    });
  }
  @override
  void initState() {
    getData();
    _dropDownBulan = buildAndGetDropDownMenuBulan();
    _dropDownTahun = buildAndGetDropDownMenuTahun();
    bulanContol.text = _dropDownBulan[0].value;
    tahunContol.text = _dropDownTahun[0].value;
    readData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:_onWillPop,
      child: Scaffold(
//      backgroundColor: Pigment.fromString('7f7fff'),
        backgroundColor: Pigment.fromString('f7f5fa'),
        appBar:AppBar(
          backgroundColor: Pigment.fromString("#6666ff"),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))),
          title: Text("History"),
          leading: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
            ),
            child: FlatButton(
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  child: Icon(IcoFontIcons.navigationMenu, size: 25,color: Colors.white),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                splashColor: Colors.amberAccent,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> OptionMenu()));
                }),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10,left: 10,right: 10),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Pigment.fromString('7f7fff'),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black12,
                        blurRadius: 2.5, // has the effect of softening the shadow
                        spreadRadius: 0.01, // has the effect of extending the shadow
                        offset: Offset(
                          2.0, // horizontal, move right 10
                          2.0, // vertical, move down 10
                        ),
                      )
                    ],
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: Text(
                                  "Nama : "
                              ,style: TextStyle(color: Colors.white)),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width/2,
                              child: Text("$namakolam",style: TextStyle(color: Colors.white),overflow: TextOverflow.ellipsis),
                            )
                          ],
                        ),

                      ),
                      Container(
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
                          ),
                          child: FlatButton(
                              padding: const EdgeInsets.all(0),
                              child: Container(
                                alignment: Alignment.center,
                                child: Icon(Icons.sort, size: 25,color: Colors.white,),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              splashColor: Colors.amberAccent,
                              onPressed: () {
                                show();
                              }),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                height: MediaQuery.of(context).size.height/1.2 - 30,
                child: FirebaseAnimatedList(
                  query: FirebaseDatabase.instance.reference(),
                  itemBuilder: (BuildContext context, DataSnapshot snapshot,
                      Animation<double> animation, int index) {
                    datahasil = snapshot.value.toString().split("/");
//                print(datahasil);
                    return filter == "" ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                      child: FlatButton(
                          color: Pigment.fromString('#e9dcfc'),
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            alignment: Alignment.center,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                              decoration: BoxDecoration(
//                            color: Colors.grey[500],
                                borderRadius: BorderRadius.circular(25),
                                border: Border.all(width: 0.3),
//                              boxShadow: [
//                                BoxShadow(
//                                  color: Colors.black12,
//                                  blurRadius: 2.5, // has the effect of softening the shadow
//                                  spreadRadius: 0.01, // has the effect of extending the shadow
//                                  offset: Offset(
//                                    2.0, // horizontal, move right 10
//                                    2.0, // vertical, move down 10
//                                  ),
//                                )
//                              ],
                              ),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context).size.width/1.5,
                                            child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text("Waktu"),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(" : "),
                                                  ),
                                                  Expanded(
                                                    flex: 9,
                                                    child: Text("${datahasil[3]}"),
                                                  ),
                                                ]),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context).size.width/1.5,
                                            child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text("History"),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(" : "),
                                                  ),
                                                  Expanded(
                                                    flex: 9,
                                                    child: Text("pH = ${datahasil[0]} ppm, Kekeruhan = ${datahasil[1]} NTU, TDS = ${datahasil[3]} ppm"),
                                                  ),
                                                ]),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context).size.width/1.5,
                                            child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text("Status"),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(" : "),
                                                  ),
                                                  Expanded(
                                                    flex: 9,
                                                    child: Text("Telah Dibersihkan"),
                                                  ),
                                                ]),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Icon(IcoFontIcons.chartPie,size: 50),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          splashColor: Colors.lightBlueAccent,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailHistory(
                              ph: datahasil[0],
                              kekeruhan: datahasil[1],
                              tds: datahasil[2],
                              waktu: datahasil[3],
                            )));
                          }),
                    ) : datahasil[3].toString().toUpperCase().contains(filter.toUpperCase()) ?
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                      child: FlatButton(
                          color: Pigment.fromString('#e9dcfc'),
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            alignment: Alignment.center,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                              decoration: BoxDecoration(
//                            color: Colors.grey[500],
                                borderRadius: BorderRadius.circular(25),
                                border: Border.all(width: 0.3),
//                              boxShadow: [
//                                BoxShadow(
//                                  color: Colors.black12,
//                                  blurRadius: 2.5, // has the effect of softening the shadow
//                                  spreadRadius: 0.01, // has the effect of extending the shadow
//                                  offset: Offset(
//                                    2.0, // horizontal, move right 10
//                                    2.0, // vertical, move down 10
//                                  ),
//                                )
//                              ],
                              ),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context).size.width/1.5,
                                            child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text("Waktu"),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(" : "),
                                                  ),
                                                  Expanded(
                                                    flex: 9,
                                                    child: Text("${datahasil[3]}"),
                                                  ),
                                                ]),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context).size.width/1.5,
                                            child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text("History"),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(" : "),
                                                  ),
                                                  Expanded(
                                                    flex: 9,
                                                    child: Text("pH = ${datahasil[0]} ppm, Kekeruhan = ${datahasil[1]} NTU, TDS = ${datahasil[3]} ppm"),
                                                  ),
                                                ]),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context).size.width/1.5,
                                            child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text("Status"),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(" : "),
                                                  ),
                                                  Expanded(
                                                    flex: 9,
                                                    child: Text("Telah Dibersihkan"),
                                                  ),
                                                ]),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Icon(IcoFontIcons.chartPie,size: 50),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          splashColor: Colors.lightBlueAccent,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailHistory(
                              ph: datahasil[0],
                              kekeruhan: datahasil[1],
                              tds: datahasil[2],
                              waktu: datahasil[3],
                            )));
                          }),
                    ) : Container();
                    /*Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailHistory(
                            ph: datahasil[0],
                            kekeruhan: datahasil[1],
                            tds: datahasil[2],
                            waktu: datahasil[3],
                          )));
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                          decoration: BoxDecoration(
                            color: Colors.grey[500],
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                offset: Offset(2, 2),
                                blurRadius: 5,
                                spreadRadius: 1,
                              )
                            ],
                          ),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context).size.width/1.5,
                                        child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 3,
                                                child: Text("Waktu"),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Text(" : "),
                                              ),
                                              Expanded(
                                                flex: 9,
                                                child: Text("${datahasil[3]}"),
                                              ),
                                            ]),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width/1.5,
                                        child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 3,
                                                child: Text("History"),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Text(" : "),
                                              ),
                                              Expanded(
                                                flex: 9,
                                                child: Text("pH = ${datahasil[0]} ppm, Kekeruhan = ${datahasil[1]} NTU, TDS = ${datahasil[3]} ppm"),
                                              ),
                                            ]),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width/1.5,
                                        child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 3,
                                                child: Text("Status"),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Text(" : "),
                                              ),
                                              Expanded(
                                                flex: 9,
                                                child: Text("Telah Dibersihkan"),
                                              ),
                                            ]),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    child: Icon(IcoFontIcons.chartPie,size: 50),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );*/
                  },
                ),
              ),
              /*Container(
                  padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                  height: MediaQuery.of(context).size.height/1.32,
                  child: alldata.length == 0 ?
                  Container(
                    child: Center(
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         Icon(Icons.cloud_off,size: 100,),
                         Text("Data Tidak Ditemukan",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
                         Text("Pastikan Device / Microcontroller terhubung ke jaringan!",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 11))
                       ],
                     )
                    )
                  )  :
                  ListView.builder(
                          shrinkWrap: true,
                          itemCount: temp.length,
                          itemBuilder: (BuildContext context, int index){
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                              child: GestureDetector(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailHistory()));
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.grey[500],
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.1),
                                        offset: Offset(2, 2),
                                        blurRadius: 5,
                                        spreadRadius: 1,
                                      )
                                    ],
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Container(
                                                width: MediaQuery.of(context).size.width/1.5,
                                                child: Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 3,
                                                        child: Text("Waktu"),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(" : "),
                                                      ),
                                                      Expanded(
                                                        flex: 9,
                                                        child: Text("${date[index]}"),
                                                      ),
                                                    ]),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context).size.width/1.5,
                                                child: Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 3,
                                                        child: Text("History"),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(" : "),
                                                      ),
                                                      Expanded(
                                                        flex: 9,
                                                        child: Text("pH = ${ph[index]} ppm, Kekeruhan = ${kekeruhan[index]} NTU, TDS = ${tds[index]} ppm"),
                                                      ),
                                                    ]),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context).size.width/1.5,
                                                child: Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 3,
                                                        child: Text("Status"),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(" : "),
                                                      ),
                                                      Expanded(
                                                        flex: 9,
                                                        child: Text("Telah Dibersihkan"),
                                                      ),
                                                    ]),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            child: Icon(IcoFontIcons.chartPie,size: 50),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          })
                ),*/
            ],
          ),
        ),
        floatingActionButton: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: Colors.red,
          ),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              child: Container(
                alignment: Alignment.center,
                child: Icon(
                  IcoFontIcons.filePdf,
                  size: 25,
                  color: Colors.white,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              splashColor: Colors.amberAccent,
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>ExportPdf(ph: ph,tds: tds,kekeruhan: kekeruhan,date: date,namakolam: namakolam,length : lengthdata)));
              }),
        ),
      ),
    );

  }
}
