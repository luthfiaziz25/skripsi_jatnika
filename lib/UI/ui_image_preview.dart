import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view.dart';

class ImageShow extends StatelessWidget {
  final image;
  ImageShow(this.image);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          child: GestureDetector(
            onVerticalDragDown: (detail) => Navigator.pop(context),
            child: PhotoView(
              imageProvider: MemoryImage(image),
              basePosition: Alignment.center,
            ),
          )
      ),
    );
  }
}
