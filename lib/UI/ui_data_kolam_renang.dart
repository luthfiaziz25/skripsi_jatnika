import 'dart:convert';
import 'dart:io';
import 'package:apmakor/Database/DatabaseLocal.dart';
import 'package:apmakor/UI/ui_image_preview.dart';
import 'package:apmakor/UI/ui_menu.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter/services.dart';
import 'package:pigment/pigment.dart';

class DataKolam extends StatefulWidget {
  @override
  _DataKolamState createState() => _DataKolamState();
}

class _DataKolamState extends State<DataKolam> {
  TextEditingController c_file = TextEditingController();
  TextEditingController c_namakolam = TextEditingController();
  TextEditingController c_panjangkolam = TextEditingController();
  TextEditingController c_lebarkolam = TextEditingController();
  TextEditingController c_kedalamankolam = TextEditingController();

  List<DropdownMenuItem<String>> _dropDownKolam;
  List _listjenisKolam = ['-','Indoor','Outdoor'];
  var jeniskolam;
  String _fileName;
  FileType _pickingType  = FileType.image;
  File _pathFile;
  File file;
  bool visible = false;
  bool nama = false;
  bool panjang = false;
  bool lebar = false;
  bool dalam = false;
  String base64Image;
  String baseFromLocal;
  var FileUpload;
  void _openFileExplorer() async {
      try {
        file = await FilePicker.getFile(type: _pickingType);
      } on PlatformException catch (e) {
        print("Unsupported operation" + e.toString());
      }
      if (!mounted) return;
      setState(() {
        _pathFile = file;
        _fileName= _pathFile.toString().split('/').last.replaceAll("'", "");
        c_file.text = _fileName;
        List<int> imageBytes = _pathFile.readAsBytesSync();
        base64Image = base64Encode(imageBytes);
        FileUpload = imageBytes;
      });
  }
  ImagePreview() {
    return _pathFile == null ? CircularProgressIndicator() : Image.file(_pathFile,fit: BoxFit.cover);
  }
  void changeJeniskolam(String selected) {
    if (selected != null || selected != '') {
      setState(() {
        jeniskolam = selected;
      });
    }
  }
  List<DropdownMenuItem<String>> buildAndGetDropDownMenuItem() {
    List<DropdownMenuItem<String>> items = new List();
    for (String data in _listjenisKolam) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(value: data, child: new Text(data)));
    }
    return items;
  }
  var datakolam;
  var stringimage;
  void saveToLocal(){
    datakolam = {
      "namakolam" : c_namakolam.text,
      "panjang" : c_panjangkolam.text,
      "lebar" : c_lebarkolam.text,
      "kedalaman" : c_kedalamankolam.text,
      "jenis" : jeniskolam.toString(),
      "namaimg" : c_file.text
    };
    stringimage = "${base64Image.toString()}";
    SaveLocalStorage().writeData(jsonEncode(datakolam));
    SaveLocalStorage().writeImage(stringimage);
  }
  void readData(){
    SaveLocalStorage().readData().then((value){
      var body = jsonDecode(value);
      setState(() {
        c_namakolam.text = body['namakolam'];
        c_panjangkolam.text = body['panjang'];
        c_lebarkolam.text = body['lebar'];
        c_kedalamankolam.text = body['kedalaman'];
        jeniskolam = body['jenis'];
        c_file.text = body['namaimg'];
        visible = true;
      });
      return value;
    });
  }
  void readImage(){
    SaveLocalStorage().readImage().then((value) async {
      print(value);
      baseFromLocal = value;
      setState(() {
        final FileFromStorage = base64Decode(baseFromLocal);
        FileUpload = FileFromStorage;
      });
      return await FileUpload;
    });
  }
  Future<bool> _onWillPop() async {
    return  showDialog(context: context,builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Pigment.fromString('#e9dcfc'),
        title: Text("Anda Ingin Keluar ?",style: TextStyle(color: Colors.black54),),
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        content: Container(
          height: 35,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.black, width: 0.1),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white,fontSize: 12),
                      ),
                    )),
              ),
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 9,
                child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 0.2),
                        borderRadius: BorderRadius.circular(10)),
                    height: 35,
                    child: FlatButton(
                      onPressed: () {
                        exit(0);
//                         _showAlertSayGoodBye(context);
                      },
                      child: Text("Lanjutkan",style: TextStyle(fontSize: 12,color: Colors.black54)),
                    )),
              )
            ],
          ),
        ),
      );
    }) ?? false;
  }
  @override
  void initState() {
    readData();
    readImage();
    _dropDownKolam = buildAndGetDropDownMenuItem();
    jeniskolam = _dropDownKolam[0].value;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
//      backgroundColor: Pigment.fromString('7f7fff'),
        backgroundColor: Pigment.fromString('f7f5fa'),
        appBar: AppBar(
          backgroundColor: Pigment.fromString("#6666ff"),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))),
          title: Text("Data Kolam Renang"),
          leading: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
            ),
            child: FlatButton(
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  child: Icon(IcoFontIcons.navigationMenu, size: 25,color: Colors.white),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                splashColor: Colors.amberAccent,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> OptionMenu()));
                }),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                GestureDetector(
                onTap: (){
                  FileUpload != null ? Navigator.push(context, MaterialPageRoute(builder: (context)=>ImageShow(FileUpload))) : Container();
                 },
                  child: Container(
                    height: MediaQuery.of(context).size.height/3,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                        image: FileUpload != null ? MemoryImage(
                            FileUpload,
                          ) : AssetImage('lib/assets/emptydata.png'),
                        fit: BoxFit.cover
                      )
                    ),
                  ),
                ),
                visible ? Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text("Picture Uploaded",style: TextStyle(color: Colors.black54)),
                      ),
                      Expanded(
                          flex: 6,
                          child:Text('${c_file.text}',textAlign: TextAlign.justify,overflow: TextOverflow.ellipsis,style: TextStyle(color: Colors.black54))
                      )
                    ],
                  ),
                ) : Container(
                  padding: EdgeInsets.only(bottom: 10,top: 10),
                  child:Stack(
                    alignment: Alignment.centerRight,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          _openFileExplorer();
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0.5),
                          child: Container(
                            width: 100,
                            padding: EdgeInsets.only(left: 20,right: 20,top: 13,bottom: 13),
                            decoration: BoxDecoration(
                              color: Pigment.fromString('#6666ff'),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Text("Browse",style: TextStyle(color: Colors.white,fontSize: 14)),
                          ),
                        ),
                      ),
                      IgnorePointer(
                        child: TextFormField(
                          controller: c_file,style: TextStyle(color: Colors.black54),
                          onTap: (){
                            _openFileExplorer();
                          },
                          decoration: new InputDecoration(
                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(35),
                              borderSide: new BorderSide(),
                            ),
                            hintText: "Select File",
                            contentPadding: EdgeInsets.only(top: 10,bottom: 10,right: 110,left: 10),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: visible ? Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text("Nama",style: TextStyle(color: Colors.black54)),
                      ),
                      Expanded(
                          flex: 6,
                          child: Text('${c_namakolam.text}',style: TextStyle(color: Colors.black54))
                      )
                    ],
                  ) : TextFormField(
                    controller: c_namakolam,style: TextStyle(color: Colors.black54),
                    decoration: new InputDecoration(
                      labelText: "Nama Kolam",
                      fillColor: Colors.white,
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(35),
                        borderSide: new BorderSide(),
                      ),
                      hintText: "Nama Kolam",
                      contentPadding: EdgeInsets.only(top: 10,bottom: 10,right: 10,left: 10),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text("Jenis Kolam Renang",style: TextStyle(color: Colors.black54)),
                      ),
                      Expanded(
                        flex: 6,
                        child: visible ? Text('${jeniskolam}',style: TextStyle(color: Colors.black54)) : Container(
                          width: MediaQuery.of(context).size.longestSide,
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: jeniskolam,style: TextStyle(color: Colors.black54),
                              items: _dropDownKolam,
                              onChanged: changeJeniskolam,
                              hint: Text("Jenis Kolam",style: TextStyle(color: Colors.black54)),
                            ),
                          ),
                          decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black38),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          padding: const EdgeInsets.only(left: 10),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text("Panjang Kolam",style: TextStyle(color: Colors.black54)),
                      ),
                      Expanded(
                        flex: 6,
                        child: visible ? Text('${c_panjangkolam.text} cm',style: TextStyle(color: Colors.black54)) : TextFormField(
                          keyboardType: TextInputType.number,
                          controller: c_panjangkolam,style: TextStyle(color: Colors.black54),
                          decoration: new InputDecoration(
                            suffix: Text(" cm ",style: TextStyle(color: Colors.black54)),
                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(35),
                              borderSide: new BorderSide(),
                            ),
                            hintText: "...",
                            contentPadding: EdgeInsets.only(top: 10,bottom: 10,right: 10,left: 10),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text("Lebar Kolam",style: TextStyle(color: Colors.black54)),
                      ),
                      Expanded(
                        flex: 6,
                        child: visible ? Text('${c_lebarkolam.text} cm',style: TextStyle(color: Colors.black54)) : TextFormField(
                          keyboardType: TextInputType.number,
                          controller: c_lebarkolam,style: TextStyle(color: Colors.black54),
                          decoration: new InputDecoration(
                            suffix: Text(" cm ",style: TextStyle(color: Colors.black54)),
                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(35),
                              borderSide: new BorderSide(),
                            ),
                            hintText: "...",
                            contentPadding: EdgeInsets.only(top: 10,bottom: 10,right: 10,left: 10),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text("Kedalaman Kolam",style: TextStyle(color: Colors.black54)),
                      ),
                      Expanded(
                        flex: 6,
                        child: visible ? Text('${c_kedalamankolam.text} cm',style: TextStyle(color: Colors.black54)) : TextFormField(
                          keyboardType: TextInputType.number,
                          controller: c_kedalamankolam,style: TextStyle(color: Colors.black54),
                          decoration: new InputDecoration(
                            suffix: Text(" cm ",style: TextStyle(color: Colors.black54)),
                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(35),
                              borderSide: new BorderSide(color: Colors.black54),
                            ),
                            hintText: "...",
                            contentPadding: EdgeInsets.only(top: 10,bottom: 10,right: 10,left: 10),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: (){
                    setState(() {
//                    print(dalam);
                      if(c_namakolam.text.length == 0 || c_kedalamankolam.text.length == 0 || c_lebarkolam.text.length == 0 || c_panjangkolam.text.length == 0){
                        Fluttertoast.showToast(msg: "Semua Field Harus Diisi",
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            toastLength: Toast.LENGTH_LONG,
                            timeInSecForIosWeb: 3);
                      }else{
                        if(visible == false){
                          visible = !visible;
                          saveToLocal();
                        }else{
                          visible = !visible;
                        }
                      }
                    });
                  },
                  color: visible ? Pigment.fromString('#6666ff') : Pigment.fromString('#6666ff'),
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 20),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    child: visible ? Text("Edit",style: TextStyle(color: Colors.white)) : Text("Save",style: TextStyle(color: Colors.white),),
                  ),
                ),
                visible ? Container() : SizedBox(height: 10),
                visible ? Container() : FlatButton(
                  onPressed: (){
                    setState(() {
                      visible = !visible;
                    });
                  },
                  color: Pigment.fromString('#e9dcfc'),
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 20),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    child: Text("Cancel",style: TextStyle(color: Colors.black54)),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

