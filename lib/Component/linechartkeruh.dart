import 'package:firebase_database/firebase_database.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class LineChartKeruh extends StatefulWidget {
  @override
  _LineChartKeruhState createState() => _LineChartKeruhState();
}

class _LineChartKeruhState extends State<LineChartKeruh> {
  List<Color> gradientColorskekeruhan = [
    Colors.red,
    Colors.orange,
  ];

  LineChartBarData data;
  List<FlSpot>valueskeruh = [];
  List dataFirebase = [];
  List dataTMP = [];
  List ph = [];
  List kekeruhan = [];
  List tds = [];
  List time = [];
  List tmptime = [];
  List finalTMP = [];
  final dbRef = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    super.initState();
  }
  Widget chart(ph,kekeruhan,tds,time) {
    valueskeruh.clear();
    if(time.isNotEmpty){
      tmptime = time.toSet().toList();
      valueskeruh.add(FlSpot(0.0,0.0));
      for(int i=0;i<tmptime.length;i++){
        if(i > 9 && (i+1) > 10){
//          valueskeruh.add(FlSpot(double.parse((i-1).toString()),double.parse(ph[i-1])));
        }else{
          valueskeruh.add(FlSpot(double.parse((i+1).toString()),double.parse(kekeruhan[i])));
        }
      }
    }else{
      valueskeruh.clear();
      valueskeruh.add(FlSpot(0.0,0.0));
    }
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        AspectRatio(
          aspectRatio: 2.0,
          child: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 4, // has the effect of softening the shadow
                    spreadRadius: 0.03, // has the effect of extending the shadow
                    offset: Offset(
                      2.0, // horizontal, move right 10
                      2.0, // vertical, move down 10
                    ),
                  )
                ],
                color: Colors.white),
            child: Padding(
              padding: const EdgeInsets.only(right: 18.0, left: 12.0, top: 24, bottom: 12),
              child: LineChart(
                LineChartData(
                  gridData: FlGridData(
                    show: true,
                    drawVerticalLine: true,
                    getDrawingHorizontalLine: (value) {
                      return FlLine(
                        color: Colors.grey[400],
                        strokeWidth: 1,
                      );
                    },
                    getDrawingVerticalLine: (value) {
                      return FlLine(
                        color: Colors.grey[400],
                        strokeWidth: 1,
                      );
                    },
                  ),
                  titlesData: FlTitlesData(
                    show: true,
                    bottomTitles: SideTitles(
                      showTitles: true,
                      reservedSize: 22,
                      textStyle:
                      const TextStyle(color: Color(0xff68737d), fontWeight: FontWeight.bold, fontSize: 16),
                      getTitles: (value) {
                        switch (value.toInt()) {
                          case 0:
                            return '0';
                          case 10:
                            return '10';
                        }
                        return '';
                      },
                      margin: 5,
                    ),
                    leftTitles: SideTitles(
                      showTitles: true,
                      textStyle: const TextStyle(
                        color: Color(0xff67727d),
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                      getTitles: (value) {
                        switch (value.toInt()) {
                          case 1:
                            return '1';
                          case 13:
                            return '13';
                          case 25:
                            return '25';
                        }
                        return '';
                      },
                      reservedSize: 28,
                      margin: 5,
                    ),
                  ),
                  borderData:
                  FlBorderData(show: true, border: Border.all(color: const Color(0xff37434d), width: 1)),
                  minX: 0,
                  maxX: 10,
                  minY: 0,
                  maxY: 25,
                  lineBarsData: [
                    LineChartBarData(
                      spots: valueskeruh,
                      isCurved: true,
                      colors: gradientColorskekeruhan,
                      barWidth: 3,
                      isStrokeCapRound: true,
                      dotData: FlDotData(
                        show: false,
                      ),
                      belowBarData: BarAreaData(
                        show: true,
                        colors: gradientColorskekeruhan.map((color) => color.withOpacity(0.3)).toList(),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 10,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Text("Grafik Kekeruhan 10 Data Terbaru",style:TextStyle(color: Colors.grey[500],fontSize: 10)),
          ),
        ),
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: dbRef.limitToLast(10).onValue,
      builder: (context, AsyncSnapshot<Event> snapshot){
        if (snapshot.hasData) {
          dataTMP.clear();
          DataSnapshot dataValues = snapshot.data.snapshot;
          if(dataValues.value != null){
            dataFirebase.clear();
            Map<dynamic, dynamic> values = dataValues.value;
            values.forEach((key, values) {
              dataFirebase.add(values);
            });
            for(int i = 0;i<dataFirebase.length;i++){
              dataTMP.add(dataFirebase[i].toString().split("/"));
            }
            dataTMP.sort((a, b) {
              return a[3].toLowerCase().compareTo(b[3].toLowerCase());
            });
            ph.clear();
            kekeruhan.clear();
            tds.clear();
            finalTMP = dataTMP.toSet().toList();
            for(int j=0;j<finalTMP.length;j++){
              ph.add(finalTMP[j][0]);
              kekeruhan.add(finalTMP[j][1]);
              tds.add(finalTMP[j][2]);
              time.add(finalTMP[j][3]);
            }
          }else{
            ph.clear();
            kekeruhan.clear();
            tds.clear();
            dataTMP.clear();
            time.clear();
          }
        }
        return chart(ph,kekeruhan,tds,time);
      },
    );
  }
}