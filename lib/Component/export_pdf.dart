import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:permission_handler/permission_handler.dart';
import 'package:share_extend/share_extend.dart';

class ExportPdf extends StatefulWidget {
  final ph;
  final kekeruhan;
  final tds;
  final date;
  final namakolam;
  final length;
  ExportPdf({Key key,this.date,this.kekeruhan,this.namakolam,this.ph,this.tds,this.length}) : super(key : key);
  @override
  _ExportPdfState createState() => _ExportPdfState();
}

class _ExportPdfState extends State<ExportPdf> {
  String pathStorageAndroid;
  String pathStorageIos;
  String fileName;
  String tanggal;
  List ph;
  List kekeruhan;
  List tds;
  List date;
  String namakolam;
  int lengthdata;

  DateTime timenow = DateTime.now();
  @override
  void initState() {
    getData();
    Export();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
          ),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              child: Container(
                alignment: Alignment.center,
                child: Icon(IcoFontIcons.close, size: 25,color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              splashColor: Colors.amberAccent,
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
      ),
      body: Center(
        child: Container(),
      ),
    );
  }

  void getData(){
    tanggal = timenow.toIso8601String().substring(0,19).replaceAll("T", "_");
    setState(() {
      ph = widget.ph;
      kekeruhan = widget.kekeruhan;
      tds = widget.tds;
      date = widget.date;
      namakolam = widget.namakolam;
      lengthdata = widget.length;
    });
  }

  void Export() async {
    var dirToSave;
    var directory;
    final pdf = pw.Document();
    pdf.addPage(pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) => <pw.Widget>[
          pw.Header(
            child: pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: <pw.Widget>[
                pw.Text("Rekap Laporan Sistem Monitoring Kolam Renang $namakolam"),
                pw.Text("Pada : ${tanggal}")
              ]
            ),),
          pw.Padding(padding: const pw.EdgeInsets.all(10)),
          pw.Container(
              child : pw.Row(
                  children: <pw.Widget>[
                    pw.Expanded(
                        flex: 2,
                        child: pw.Text("No")
                    ),
                    pw.Expanded(
                        flex: 3,
                        child: pw.Text("Nilai pH")
                    ),
                    pw.Expanded(
                        flex: 4,
                        child: pw.Text("Nilai TDS")
                    ),
                    pw.Expanded(
                        flex: 5,
                        child: pw.Text("Nilai Kekeruhan")
                    ),
                    pw.Expanded(
                        flex: 4,
                        child: pw.Text("Waktu")
                    ),
                    pw.Expanded(
                        flex: 4,
                        child: pw.Text("Status")
                    ),
                  ]
              )
          ),
          pw.Container(
              child: pw.ListView.builder(
                  itemCount: lengthdata,
                  itemBuilder:(pw.Context context,int index){
                    return pw.Container(
                        child : pw.Row(
                          mainAxisAlignment: pw.MainAxisAlignment.center,
                            children: <pw.Widget>[
                              pw.Expanded(
                                  flex: 2,
                                  child: pw.Text("${index+1}")
                              ),
                              pw.Expanded(
                                  flex: 3,
                                  child: pw.Text("${ph[index]}")
                              ),
                              pw.Expanded(
                                  flex: 4,
                                  child: pw.Text("${kekeruhan[index]}")
                              ),
                              pw.Expanded(
                                  flex: 5,
                                  child: pw.Text("${tds[index]}")
                              ),
                              pw.Expanded(
                                  flex: 4,
                                  child: pw.Text("${date[index]}")
                              ),
                              pw.Expanded(
                                  flex: 4,
                                  child: pw.Text("Status Dummy")
                              ),
                            ]
                        )
                    );
                  })
          ),
          pw.Padding(padding: const pw.EdgeInsets.all(10)),
          pw.Container(
            child: pw.Column(
              children: <pw.Widget>[
                pw.Row(
                  children: <pw.Widget>[
                    pw.Expanded(flex: 3,
                        child: pw.Text("Deskripsi ",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 1,
                        child: pw.Text(":",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 6,
                        child: pw.Text("Isi ",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                  ],
                ),
              ],
            ),
          ),
        ]));
    if (Platform.isIOS) {
      dirToSave = await getApplicationDocumentsDirectory();
      directory = dirToSave.path;
      setState(() {
        pathStorageIos = "${directory}/";
      });
    } else{
      final PermissionHandler _permissionHandler = PermissionHandler();
      var result = await _permissionHandler.requestPermissions([PermissionGroup.storage]);
      if (result[PermissionGroup.storage] != PermissionStatus.granted) {
        Fluttertoast.showToast(
            msg: "Download Failed, Access Not granted 1",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb : 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
//        pr.hide();
        return;
      }
//      directory = await getExternalStorageDirectory();
//      directory = await getTemporaryDirectory();
      directory = '/storage/emulated/0/Download';
      setState(() {
        pathStorageAndroid = "${directory}/";
      });
    }
    final File file = File('${directory}/ApmakorReport_${tanggal}.pdf');
    fileName = "ApmakorReport_${tanggal}.pdf";
    await file.writeAsBytes(pdf.save()).then((e){
      Navigator.pop(context);
      _showOptionToOpenFile(context,file);
    }).catchError((f){
      print(f);
      Fluttertoast.showToast(
          msg: "Download Failed, Access Not granted",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb : 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    });
  }

  void _showOptionToOpenFile(context,path){
    String tmpfile = path.toString();
    int lengthtmp = tmpfile.length;
    String file = path.toString().substring(7,lengthtmp-1);
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Berhasil Disimpan"),
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
//                    color: Colors.amber,
                  ),
                  child: FlatButton(
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Icon(Icons.close,size: 25,color: Colors.black  ,),
                      ),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                      splashColor: Colors.amberAccent,
                      onPressed: (){
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 135,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 90,
                    child: Text("Tersimpan Di ${file}",textAlign: TextAlign.left,maxLines: 4,overflow: TextOverflow.ellipsis,),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                color: Colors.red,
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                if(Platform.isIOS){
                                  ShareExtend.share(pathStorageIos + fileName,"file");
                                }else{
                                  ShareExtend.share(pathStorageAndroid + fileName,"file");
                                }
                              },
                              child: Text("Share",style: TextStyle(color: Colors.white),),
                            )
                        ),
                      ),
                      Expanded(flex: 1,
                          child: Container()),
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                if(Platform.isIOS){
                                  OpenFile.open(pathStorageIos + fileName);
                                }else{
                                  OpenFile.open(pathStorageAndroid + fileName);
                                }
                              },
                              child: Text("Open"),
                            )
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }
}
