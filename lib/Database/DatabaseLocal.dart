import 'dart:io';
import 'package:path_provider/path_provider.dart';

class SaveLocalStorage {
  Future<String> get localPath async {
    final dir = await getApplicationDocumentsDirectory();
    return dir.path;
  }

  Future<File> get localData async {
    final path = await localPath;
    return File('$path/datakolam.txt');
  }

  Future<File> get localImage async {
    final path = await localPath;
    return File('$path/gambarkolam.txt');
  }

  Future<String> readData() async {
    try {
      final file = await localData;
      String body = await file.readAsString();
      return body;
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> readImage() async {
    try {
      final file = await localImage;
      String body = await file.readAsString();
      return body;
    } catch (e) {
      return e.toString();
    }
  }

  Future<File> writeData(String data) async {
    final file = await localData;
    return file.writeAsString("$data");
  }

  Future<File> writeImage(String data) async {
    final file = await localImage;
    return file.writeAsString("$data");
  }
}