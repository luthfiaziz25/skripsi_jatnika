import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class sendnotif{
  final firebaseMessaging = FirebaseMessaging();
  var url = 'https://fcm.googleapis.com/fcm/send';
  Future<bool> dangernotif() async {
    var body = {
      "notification" :{
        "title" : "Perhatian",
        "body" : "Kolam Renang dalm Keadaan Tidak Layak, Silakan Lakukan Perawatan",
        "click_action" : "FLUTTER_NOTIFICATION_CLICK"
      },
      "to" : "/topics/apmakor"
    };

    var key = 'key=AAAAD-XhYd0:APA91bHoSUuo1EvMkHvyaiXcoavtzSPb4_OphQFIX8SYAjEM2W2K8t4HHfY7tFkE57fwXCLheGxc2UHPqdlj4VmzLGHp4PsFFMxZkYqOXG-ZAnLUh7z2RtzUIKuHRcXCTVtY9Ncf29L8';

    var dio = Dio();
    dio.options.headers['Authorization'] = key;
    dio.options.headers['content-Type'] = 'application/json';
    Response response = await dio.post(url,data: body);
    try{
      print("ini respon"+response.toString());
    }catch(e){
      print(e);
    }
  }
}